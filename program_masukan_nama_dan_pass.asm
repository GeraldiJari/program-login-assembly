.MODEL SMALL
.CODE
ORG 100h

tdata:  
jmp proses
    password    db ' '
    password1   db 20 dup(?)
    Kata1       db "PROGRAM MEMASUKKAN NAMA DAN PASSWORD $"
    
    username    db 13,10,'Username : $'
    pass        db 13,10,'Password : $'
    ditolak     db 13,10,'Ditolak $'
    diterima    db 13,10,'Anda Berhasi Login :) $'
  
    username1   db 23,?,23 dup(?)
    pass        db 23,?,23 dup(?)
proses:
        mov ah,09h
        mov dx,offset Kata1
        int 21h
    
        mov ah,09h 
        lea dx,username
        int 21h

        mov ah,0ah  
        lea dx,username1
        int 21h

        MOV AH,9
        LEA DX,pass
        INT 21H
    
        MOV CX,11
        
INPUT:  MOV AH,07
        INT 21H
    
        MOV [DI],AL
        INC DI
    
        MOV AH,2
        MOV DL,'*'   ;penulisan bintang ketika proses input
        INT 21H
    
        LOOP INPUT
    
        LEA SI,password
        LEA DI,password1
    
        MOV CX,11
    
        MOV BX,0
    
 Y     :MOV BL,[SI]
        MOV BH,[DI]
    
        INC SI
        INC DI
    
        CMP BL,BH
        JNE X
        LOOP Y
    
        MOV AH,9
        LEA DX,ditolak
        INT 21H
    
        JMP EXIT
    
 X:     MOV AH,9
        LEA DX,diterima
        INT 21H
    
 EXIT:  MOV AH,4CH
        INT 21H
        

        END tdata
        
    
    
